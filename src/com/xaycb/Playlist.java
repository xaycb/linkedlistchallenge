package com.xaycb;

import java.util.LinkedList;

public class Playlist {
    private String name;
    private LinkedList<Song> playlist;

    public Playlist(String name) {
        this.name = name;
        this.playlist = new LinkedList<>();
    }
    public void addSong(Album album, String songName){
        Song checkedSong = album.findSong(songName);

        if (checkedSong != null){
            playlist.add(checkedSong);
        } else {
            System.out.println("No such song in album");
        }
    }

    public void printPlaylist(){
        System.out.println("Playlist " + name + ":");
        for (int i = 0; i < playlist.size(); i++) {
            System.out.println(playlist.get(i).toString());
        }
    }

}
