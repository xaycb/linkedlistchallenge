package com.xaycb;

public class Song {
    private String title;
    private double duration;

    public Song(String title, double duration) {
        this.title = title;
        this.duration = duration;
    }

    public boolean checkName(String name){
        return this.title == name;
    }

    @Override
    public String toString() {
        return this.title + ": " + this.duration;
    }
}
