package com.xaycb;

import java.util.ArrayList;

public class Album {
    private String name;
    private ArrayList<Song> songList;

    public Album(String name) {
        this.name = name;
        this.songList = new ArrayList<Song>();
    }

    public void addSong(String title, double duration){
        if (findSong(title) == null){
            this.songList.add(new Song(title, duration));
        } else {
            System.out.println("Song already exist");
        }
    }

    public Song findSong(String name){
        for (int i = 0; i < this.songList.size(); i++) {
            if (this.songList.get(i).checkName(name)){
                return this.songList.get(i);
            }
        }
        return null;
    }
    public void songList(){
        System.out.println("Album " + this.name + " contains following songs:\n");
        for (int i = 0; i < songList.size(); i++) {
            System.out.println(songList.get(i).toString());
        }
        System.out.println();
    }

}

