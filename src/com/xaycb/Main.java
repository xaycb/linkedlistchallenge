package com.xaycb;

public class Main {

    public static void main(String[]  args) {
	// write your code here

        Album meteora = new Album("meteora");
        Album chuck = new Album("chuck");
        Playlist playlist = new Playlist("xaycb");

        meteora.addSong("Numb", 3.43);
        meteora.addSong("Waiting", 2.21);
        meteora.addSong("Breaking", 4.01);
        chuck.addSong("people", 2.34);
        playlist.addSong(meteora, "Numb");
        playlist.addSong(meteora, "Numb");
        playlist.addSong(chuck, "people");

        meteora.songList();
        chuck.songList();

        playlist.printPlaylist();
    }
}
